---
template: post
slug: /migrate-to-gatsby
title: Jekyll에서 Gatsby로
date: '2018-05-12'
category: "dev"
tags:
draft: false
description: 송아지를 들이려고 외양간을 고친 이야기
---

### 블로그에 TIL 섹션을 넣고 싶어졌다

최근 삽질한 기록들을 TIL이라는 리포를 파서 기록하기 시작했다. 기왕이면 이 내용들이 구글에서 검색되었으면 좋겠는데 - 누군가에게는 도움이 될 수 있으니 - 그렇다고 이 리포를 퍼블릭으로 만들자니 괜히 커밋 타일이 초록색으로 물드는 것 같아서 꺼려졌다. 오픈소스 활동은 하지도 않는데 타일만 푸르다면 그게 무슨 의미가 있을까. 결국 블로그에 TIL 섹션을 추가해서 포스트 리스트에는 뜨지 않지만, 어쨌든 접근은 가능한 상태로 만들기로 했다.

### Jekyll을 떠나서 Gatsby 로

예전 블로그는 Jekyll을 사용했는데, TIL 섹션을 추가하려고 Jekyll을 배우자니 Ruby랑 친하지 않아서 부담이 많이 갔다. 그래서 최근 많이 언급되는 툴들인 hugo, hexo, gatsby, metalsmith 중에서 Gatsby가 제일 힙해보여서 이걸로 블로그를 다시 만들기로 했다. 항상 감사하게 쓰고 있는 Netlify에서 JAM 스택을 미는 것도 있고, JavaScript와 React에 비교적 친숙하기도 해서 선택이 어렵지는 않았다.

### Gatsby랑 친해지기

Gatsby는 정적 사이트 생성기다. 콘텐츠는 동적인데 웹사이트가 정적이려면 결국 특정 시점을 기준으로 컨텐츠들을 박제한 후 파일들을 빌드하면 된다. Jekyll에서는 markdown 파일이 그 컨텐츠라면 Gatsby는 마크다운 파일 뿐만 아니라 더 다양한 소스, Wordpress 나 Contentful 등의 CMS 시스템, 아니면 임의의 API 에서 데이터를 불러와서 정적 사이트를 생성할 수 있다. 이렇게 로드된 데이터는 GraphQL을 통해 각각의 페이지로 주입이 되는데, 이래저래 Jekyll로 처음 블로그를 만들 때 (그때는 그냥 다른 리포를 클론해서 markdown 파일들만 채웠다)에 비해서 약간의 학습이 필요했다. 튜토리얼이 정말 잘 쓰여져있어 감탄하면서 읽었다.

### 포스팅 옮기기

[Lumen](https://github.com/alxshelepenok/gatsby-starter-lumen)이란 템플릿에 블로그를 옮겼는데, 이 템플릿은 Jekyll의 [frontmatter](https://jekyllrb.com/docs/frontmatter/) 를 적극적으로 활용하는 편이었다. 기존에 작성된 markdown 파일들에는 `title`, `layout` 정도를 제외하고는 frontmatter 정보가 거의 작성되있지 않았기 때문에 포스팅을 그대로 옮기고 나니 처음에 node를 생성하고 레이아웃과 페이지를 만들 때 에러가 발생했다. 여기서 두가지 선택지가 있었다.

1.  Lumen에서 사용하는 frontmatter 필드를 다 넣어주기
2.  Gatsby 튜토리얼 중 마지막인, [Programmatically create pages from data | GatsbyJS](https://www.gatsbyjs.org/tutorial/part-seven/#creating-slugs-for-pages) 을 따라서 onCreateNode 에서 파일명에 따라 frontmatter 정보를 바꿔주기.

애초에 Jekyll 에서 Gatsby 로 옮긴 이유가 좀 편하게 Customize 를 하기 위해서였으니까 호기롭게 2번 방안을 시도했는데, Gatsby 에서 Node 는 immutable 한 객체라서 수정이 안되고 [createNodeField](https://www.gatsbyjs.org/docs/bound-action-creators/#createNodeField)를 사용해서 필드를 추가하는 경우만 가능했다. 그러면 node.frontmatter 객체에 createNodeField 를 하면 되지 않을까 싶었는데 잘 되지 않았고, 아쉽지만 노가다로 1번 방안을 택했다. 어차피 마크다운 기반의 정적사이트 생성기는 다 frontmatter를 사용할테니 아주 시간을 버린 것은 아니지만, 기분이 계속 찝찝했다.

### TIL Node 만들기

~드디어 원래 목적인 TIL 섹션을 추가할 차례다. TIL은 다른 리포에 쓰고 있는데, 이걸 블로그 리포 안에 넣을 수는 없으니 git submodule 로 TIL 에 있는 마크다운 파일들을 클론하기로 했다. TIL은 프라이빗 리포기 때문에 Netlify에서 clone이 안되서.. 환경변수로 깃헙 아이디랑 비번을 읽어서 pull 하는 간단한 스크립트를 붙였다. 여전히 좋은 방법인지는 확신이 안가지만 데이터와 로직이 분리되었음에 만족하기로 한다~

그냥 TIL 리포를 GitLab으로 옮겼다. 아래와 같은 스크립트도 필요없고, GitHub 타일이 푸르게 변할 일도 없고 문제가 다 풀림.

```sh
{
if [[ -z "${GIT_ID}" ]]; then
  echo "pull.sh: No GitHub ID and PASS in environemnt variables"
  GIT_REPO_URL=github.com/zxzl/til.git
else
  echo "pull.sh: Using GitHub ID and PASS from environment variables"
  GIT_REPO_URL=$GIT_ID:$GIT_PASS@github.com/zxzl/til.git
fi
cd til
git pull https://$GIT_REPO_URL
cd ../
git submodule update
}
```

라고 `pull.sh`를 만든 다음에 빌드 스크립트로 `pull.sh && gatsby build`를 쓰면, 어쨌든 현재 submodule이 담는 커밋에 상관없이 최신 정보로 빌드가 되긴 한다.

이렇게 pull된 til 폴더에서도 파일을 불러오는 플러그인을 추가한다.


```json
[
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages'
      }
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/til`,
        name: 'til'
      }
    }
]
```
블로그 포스팅은 사진도 있으니까 폴더를 생성하고 그 안에서 마크다운을 작성하는데, TIL은 그냥 마크다운 파일들이 하나씩 있는 형태다.
따라서 slug를 폴더명이 아닌 파일 명에서 뽑아내도록 onCreateNode를 고쳐준다.

### TIL 섹션 만들기

이제 저 node들을 쿼리해서, 페이지들을 만들고 이 페이지들을 모아놓은 리스트를 만들 차례다. 여기서 Gatsby (혹은 GraphQL)의 편리함이 느껴지는데,
그냥 원래 Index 페이지에서는 category가 til이 아닌 것만 넣도록 쿼리를 수정하고, TIL 페이지에서는 category가 til인 마크다운 파일만 넣도록 쿼리를 작성하면 된다. 사실 Hexo 같은 거에서도 될 것 같은데 왠지 이게 더 직관적인 것 같다 (GraphQL이랑 친한척?). 아쉽지만 오늘은 이정도로 끝내기로.

### 느낀점

* 글이나 디자인을 고칠 때 HMR이 기본으로 지원되니까 엄청 편하다.
* 마크다운 파일도 node고, 그걸 렌더한 결과도 node라는 개념을 이해해야 한다.
* 플러그인과 onCreateNode를 거쳐서 GraphQL로 쿼리 가능한 API가 생성되고, 그걸 가지고 보통의 React 앱을 만든다고 생각하면 대충 맞다.
* 솔직히 마크다운만 다룰 거면(=이 블로그) Hexo가 훨씬 간단할 것 같은데, 워드프레스나 다른 CMS에서 컨텐츠가 바뀔 때마다 바로바로 빌드되는 시스템이나, 아니면 인터랙션이 많은 사이트를 고려하면 이게 좋겠다. React 생태계의 모든 것을 바로 가져다 쓸 수 있다.
* 고로 [Contentful](https://www.contentful.com/)이나 [Netlify-CMS](https://github.com/netlify/netlify-cms) 같은 툴도 같이 익혀놓으면 좋겠다. 특히 개발자가 아닌 분들이랑 일할 경우 더더욱.
* Netlify-CMS를 잠시 시험해 보았는데, 개인적으로는 VSCode에서 마크다운을 편집하고 옆의 탭에서 Preview를 보는 것에 비해서 딱히 장점을 느끼지 못했다. 또한 Netlify Identify에 종속적이다. 컨셉은 재밌는데..왠지 개발자는 그냥 로컬에서 작업하고 비개발자는 더 유저 친화적인 CMS를 쓸 것 같다.

### 참고한 글들

* [Migrating The Blog From Jekyll To Gatsby](http://unlikenesses.com/2017-11-06-migrating-blog-to-gatsby/)
* [Is there a way to access the fields created by the gatsby-remark-transformer plugin?](https://github.com/gatsbyjs/gatsby/issues/2991)
