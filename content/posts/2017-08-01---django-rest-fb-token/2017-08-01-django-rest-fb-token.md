---
template: post
slug: /django-rest-fb-token
title: 장고에서 페이스북 기반 토큰 인증 사용하기
date: '2017-08-01'
category: "dev"
tags:
draft: false
---

예전부터 장고를 사용하면 코딩을 거의 하지 않고서도 인증을 구현할 수 있지 않을까하는 환상이 있었는데, 마침 장고를 이용하는 도구에 인증을 붙일 일이 생겨서 그 환상을 실현할 기회를 얻었다.

결론부터 말하자면 코딩은 정말 한 줄도 하지 않았다. 대신 평소 장고에 익숙하지 않아서 여러 튜토리얼과 질문/답변을 옮겨다니면서 코드를 참조했기에 그 과정을 미래의 나를 위해 하나의 튜토리얼로 남겨본다. 각 단계별로 링크된 튜토리얼을 따라 했다는 전제하에, 내가 빠뜨렸던 부분을 첨언하는 식으로 튜토리얼을 구성해보았다.

##### 1. Django REST framework (이하 DRF) 프로젝트 만들기 - [튜토리얼](http://www.django-rest-framework.org/tutorial/quickstart/) 참조.

뭔가 RESTful 하게 보여줄 것이 있어야하니까 장고 프로젝트를 만들면 생기는 User 를 위한 Serializer 를 만든다. 쭉 따라가다가 [URL ](http://www.django-rest-framework.org/tutorial/quickstart/#urls) 부분에서 약간 차이점이 생긴다. DRF 튜토리얼에서는 admin 을 사용하지 않는데, 여기서는 나중에 django-allauth 에 페이스북 앱을 설정해야하기 때문에 admin 을 남겨줘야 한다. 그러므로 원래 코드에 두 줄 추가해서

```python
from django.contrib import admin #어드민
from django.conf.urls import url, include
from rest_framework import routers
from tutorial.quickstart import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^admin/', admin.site.urls), #어드민
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
```

`url.py`를 이렇게 만들어준다. 계속 튜토리얼을 따라가본다.

##### 2. django-rest-auth 붙이기

마찬가지로 [튜토리얼](http://django-rest-auth.readthedocs.io/en/latest/installation.html) 을 따라간다. 시간을 다소 많이 쓴 부분은
[Social Authentication](http://django-rest-auth.readthedocs.io/en/latest/installation.html#social-authentication-optional) 부분의 _Add Social Application in django admin panel_ 부분이었는데, 이 라이브러리가 의존하고 있는 [django-allauth](http://django-allauth.readthedocs.io/en/latest/index.html) 를 한 번도 사용한 적이 없어서 많이 헤맸다. 무서워하지말고 django-allauth 의 튜토리얼을 따라 가본다. 우리는 토큰 기반의 인증을 사용할 테니 `settings.py`에

```python
REST_SESSION_LOGIN = False

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    )
}
```

이런 식으로 설정해준다.

##### 3. django-allauth 에서 설정하기

다시 또 [튜토리얼](http://django-allauth.readthedocs.io/en/latest/istallation.html) 을 따라간다. 페이스북 콘솔에서 앱 등록하는 부분은 패스. `SITE_ID`라는 개념이 생소할 수 있는데 어차피 지금은 사이트를 여러개 사용하는 것이 아니니 어드민에서 sites 로 들어가 처음에 들어있는 'example.com'을 사용할 hostname 으로 바꿔주면 된다.

##### 4. django-allauth 에서 페이스북 설정하기 [링크](http://django-allauth.readthedocs.io/en/latest/providers.html#facebook)

django-allauth 는 다양한 서비스의 로그인을 지원하는데, 우리는 그 중 페이스북을 사용할 것이므로 링크를 따라 `settings.py`에 설정을 넣어준다. 예제에는 항목이 많이 나와있는데 현재 용도에서는

```python
SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'SCOPE': ['email'],
        'METHOD': 'oauth2'
    }
}
```

이정도만 써줘도 무리가 없다.

##### 5. 써보기

자바스크립트로 Facebook 로그인 버튼을 만든 다음에 (React 에서는 [React Facebook Login](https://github.com/keppelen/react-facebook-login)이 간편하다) 로그인 성공시 받은 토큰으로 `rest-auth/facebook/` 에 POST 요청을 날려주면 된다. 헤더의 Authroization 란에 토큰을 넣을 때에는 "Token XXX" (공백 포함) 식으로 넣어줘야 한다.

이건 말 그대로 겉핥기고, 본격적으로 무언가를 만드려면 장고의 유저 모델과 인증에 대해서 공부를 훨씬 더 많이 해야할 것 같다. 그래도 일단 한 번 익숙해지면 개발 속도가 빠르니 장고를 주력으로 사용하시는 분들은 생산성이 정말 어마어마하시겠구나 싶다.
