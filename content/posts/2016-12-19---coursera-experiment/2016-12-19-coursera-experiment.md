---
template: post
title: Coursera 실험 수업
date: '2016-12-19'
category: "research"
slug: /coursera-experiment
tags:
draft: false
---

올 가을부터 Coursera 에 [Interaction Design](https://www.coursera.org/specializations/interaction-design)이라는 specialization 이 생겼는데 그 중 [Designing, Running, and Analyzing Experiments](https://www.coursera.org/learn/designexperiments/) 파트는 어디가서 배우기 힘든 내용이라고 교수님이 듣기를 강력 추천하셨지만 당시에는 듣지 않았다(죄송합니다...). 그러다 요즘 학기와 겨울 방학 사이 약간의 틈이 생겨 9 주짜리 코스지만 집중적으로 듣고, 그중에 좀 남길 만한 내용을 남겨보려고 한다.

---

**Week1**에는 실험이 왜 필요한지와 실험 디자인에 대한 큰 그림이 나온다.

### 실험이 왜 필요한가

우리가 실험을 해야하는 이유는 디자인을 수치적으로 이해하고, 증명해서 결국 디자인을 개선하는데 목적이 있다. 그 측정의 대상은 **Performance**가 될 수도 있고, **Preference**가 될 수도 있다. 또한 실험은 **Exploratory** 할 수도 있고, 아니면 좀 더 구체적인 **Hypothesis&**를 검증하는 방식으로 진행될 수도 있다. 강의에서는 웹사이트의 디자인을 개편했을 때 사용자의 선호도를 알아보기 위한 A/B Test 를 예로 들었는데, 사실 이 경우 측정 오차 때문에 두 개의 같은 디자인을 놓고 실험해도 차이가 나타날 수 있다. (이런 효과를 측정하기 위한 실험을 [A/A Testing](https://www.optimizely.com/optimization-glossary/aa-testing/)이라고 한다. [Ron Kohavi 님의 저술](http://ai.stanford.edu/~ronnyk/ronnyk-bib.html)들 중에 이 쪽으로 재밌는 내용이 많다)

### 실험 디자인의 큰 그림

강연자가 슬라이드 하나 없이 앞에서 쭉 강의해서 (일단 퀴즈를 풀기 위해) Bullet Point 로 정리했다.

#### Statistical significance vs. Practical significance

* Statistical significance: mathematical probability that a relationship btw. two or more variable exists
  * Eg. Testing preference: We need hypothesis and test statistic. It returns confidence
* Practical significance: actual difference it is estimating will affect a decision to be made

#### 4 Major considerations for every experiment

1.  Participants
    * Sampling: how do they become part of our study?
      * purpose: Make composition of sample similar to real
    * Probability sampling: consisting randomness
    * Non-probability sampling
      * Convenience sampling: At researcher's convenience
      * Snowball sampling: Random first sample -> he/she introduce next sample
    * Inclusion / Exclusion criteria
      * If we experiment users's preference between old & new design of web site, we may exclude people who are already familiar with old design
2.  Apparatus
    * What do we need in terms of equipment, space, and other resources
    * Remote(same time, different place) / online study(different time and place)
    * Do we build something? how to capture data? (human? video? log? )
3.  Procedure
    * What do they actually go through as they come into the study
    * There might be some side effect (fatigue effect, learning effect)
    * Informed consent is very important!: something similar to IRB
      * Don't forget you're in charge of their welfare during that time
      * Is there any potential risk? Space should be accessible for blind, deaf, etc...
      * At the end of study, you have to debrief your study to participants
    * You may have seperate MC for proceeding experiment, while other one recording data.
4.  Design and analysis
    * What do they do on each site

---

P.S

* Week2 부터는 자주 쓰이는 분석을 R 로 하나씩 해본다고 하는데, 언젠가 프로덕트에 넣을 때를 생각해서 Python 기반의 툴로 따라갈 수는 없을까 궁리중이다. 이건 들어보고 업데이트.
* 강의를 조금 들어보니 어떤 어떤 경우에는 X 분석을 돌리세요. X 분석은 이런거에요. 식으로 강의가 흘러간다. R 로 하는 일이라고 해봤자 어차피 데이터를 처리해서 블록킹 연산 하나에 넣는거고, 별로 무거울 것도 없어서 Python 기반으로 하는 건 필요할 때 해도 충분히 하겠다 싶었다.
