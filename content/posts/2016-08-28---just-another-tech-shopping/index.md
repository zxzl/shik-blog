---
template: post
title: https @ GitHub Page + Custom Domain
slug: '/just-another-tech-shopping'
date: '2016-08-28'
category: "dev"
tags:
draft: false
---

Https 가 아닌 http 는 거의 폐기물 취급을 받는 최근의 추세에 따라 이 공간에도 https 를 적용하고자 방법을 찾아보았는데, GitHub Page 자체에서는 Custom Domain 을 위한 https 연결은 아직 지원하지 않고, 오히려 GitLab Page 가 [지원을 한다고 한다](https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/). 예전에 GitHub 에서 학생팩을 받으면서 GitLab 계정을 지워버렸기에 이건 패스. 대신 CloudFlare 에서 자신들의 서비스를 깃헙 페이지에 물리면 손쉽게 https 지원이 가능하다고 친절히 [튜토리얼](https://blog.cloudflare.com/secure-and-fast-github-pages-with-cloudflare/)을 제공해서 그대로 따라해보았고 당연히 잘 되었다. 우려되었던 점이 다소 있었는데, 기우에 불과했다.

1.  DNS 를 바꿔야 한다. <= 원래 도메인을 구입하던 곳에서 제공하는 DNS 를 이용하고 있었는데, 여기 인터페이스가 훨씬 편하다. 당연히 CDN 안쓰고 DNS 로만 이용하는 기능도 있기 때문에 DNS 서버를 바꾸는데 크게 고민할 필요는 없을 거 같다.
2.  CDN 을 사용하게 되면 업데이트가 느리지않을까 하는 불안감을 가질 수 있는데 Caching 탭에 보면
    ![alert](./cloudflare-dev-mode.png)
    이렇게 잠시 캐싱을 꺼놓는 기능이 있어서 그마저도 별 탈이 없었다.

그렇게해서 이 블로그에도 초록색 자물쇠가 걸리게 되었다. 최근에 다시 들어가본 Firebase 에서도 호스팅은 당연히 https 를 [지원하는등](https://firebase.google.com/docs/hosting/) 적어도 정적호스팅에 관해서는 https 때문에 고민할 일이 거의 없을 것 같다.

이렇게 오늘도 기술의 소비자가 되어간다^^

---

**Update 2016-11-30**
약간 로딩이 느린 느낌이 있어서 개발자도구로 봤더니
![inspect](./inspect.png)
TTFB 가 안습...ㅜㅜ 페이지가 미국에 있는 엣지에서 제공되다보니 어쩔 수 없는 현상 같다. 유료 플랜을 사용하면 서울에 있는 엣지에서 호스팅된다고 한다.

**Update 2016-12-19**
GitHub 에 올린 다른 repo 의 페이지까지 `blog.shik.be`도메인을 적용하기가 좀 그래서 그냥 zxzl.github.io 도메인을 사용하기로. 내용이 중요하지 도메인이 중요하겠는가.

**Update 2018-01-22**
삽질하지 말고 [Netlify](https://www.netlify.com) 쓰면 된다
