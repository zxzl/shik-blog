---
template: post
title: Logitech MX Anywhere2 사용기
date: '2016-11-30'
category: "etc"
slug: /logitech-mx-anywhere-2
tags:
draft: false
description: 이 마우스는 애플 매직마우스를 대체할 수 있다
---

맥북을 모니터와 연결해서 사용할 때 맥북을 모니터 앞에 놓아 키보드와 트랙패드를 그대로 이용할 수도 있고, 아니면 맥북은 옆의 스탠드에 올려 놓고 별도의 키보드와 마우스를 사용할 수도 있다. 개인적으로 맥북 키보드에서 느껴지는 은은한 발열을 별로 좋아하지 않아서(외부 모니터를 연결할 경우 화면을 더 그려야되니 열이 더 난다. 물론 작업 by 작업) 될 수 있으면 별도의 키보드와 마우스를 사용하는 편이다. 지금까지는 로지텍에서 나온 만원짜리 무선 마우스를 사용하며 본의 아니게 트랙패드의 제스처에 해당하는 키보드 단축키를 외워버렸는데, 이번 블랙프라이데이에 영국 아마존에 로지텍의 상위 라인 마우스들이 저렴하게 나와서 MX Anywhere2 와 MX Master 세트를 10 만원 가량에 구입했다. MX Master 는 그 특유의 '나 프로임'하는 자태가 부담스러워 65,000 원 가량에 <del>미개봉</del>중고로 팔았으니 이 마우스 자체는 35,000 원에 구입한 셈이다.

보통의 마우스가 좌클릭, 우클릭, 위/아래 입력을 지원하는 휠 이렇게 4 종류의 입력을 받는다면 이 마우스는 그에 더해서 좌/우로 꺾이는 휠버튼, 휠 뒤의 제스처버튼, 엄지손가락 부분의 버튼 2 개까지 5 종류의 입력을 더 받는다. 이 특수 버튼들을 용도에 맞게 잘 매핑할 경우 애플의 매직 마우스 못지 않게 편리하게 쓸 수 있다. 개인적인 설정은 다음과 같다.

* 좌/우 휠버튼: MacOS 의 _페이지 쓸어넘기기_
* 제스처버튼: 원래는 제스처 버튼인데 자주 누르기가 힘들어서 *데스크탑 보기*로 설정해 놓았다.
* 엄지쪽 버튼 1: 제스처버튼. 이 버튼을 누르고 상/하/좌/우로 마우스를 움직이면 트래패드에서 손가락 4 개를 같은 방향으로 움직였을 때와 같은 동작을 한다. 움직임 없이 버튼만 눌렀을 때는 미션컨트롤이 실행되도록 해놓았다.
* 엄지쪽 버튼 2: 뒤로가기.

몇 일 동안 사용하다보니 원래 손가락 여러 개를 트랙패드 위에서 움직이는 것보다 버튼을 누르는 것이 수고가 적게 든다는 팩트를 깨달으며 이 마우스에 완전히 적응해버렸다. 한가지 불만이라면 제스처 버튼을 이용해서 좌우로 제스처를 넣어야할 때 마우스를 다소 많이 움직여야한다는 점? 트랙패드의 그 쫄깃한 느낌과는 확실히 거리가 있고 별도로 감도를 설정할 수도 없다. 나는 화면 전환을 휠을 좌우로 기울여서하기 때문에 큰 단점으로 다가오지는 않다만, 기기의 전반적인 완성도를 크게 깎아먹는 느낌이다. 이 기능에 대한 사용자의 눈높이가 매직마우스나 맥북의 트랙패드에 있음을 고려하면 더더욱 그렇다.

그럼에도 불구하고, MX Anywhere 2 는 하루종일 PC 앞에 있는 사람들에게 정말 추천하고 싶은 마우스다. USB 수신기/블루투스 모두 지원하는 점도 장점이고, 라이트닝 케이블이 아닌 마이크로 5 핀으로 충전이 된다는 점도 내게는 장점이다. 3 대까지 멀티페어링이 가능한 점도 이 마우스를 여러 기기에 물려쓸 사람들에게는 큰 장점일 것이다. 나는 이 가격에 샀으니 정말 더이상 바랄게 없고 시중가인 7 만원도 사용해보고나니 충분히 수긍이 간다.
