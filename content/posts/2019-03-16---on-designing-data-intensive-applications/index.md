---
title: Designing Data Intensive Applications를 읽고
date: "2019-03-16"
template: post
slug: /on-designing-data-intensive-applications
draft: false
category: "dev"
description: 매년 다시 읽어야 할 책
tags:
---

우선 개인적으로 이 책을 100% 이해를 못했음을 미리 밝히고, 우선 다른 분들이 써주신 좋은 리뷰들의 링크를 남긴다. 이 책의 공식 트위터 [계정](https://twitter.com/intensivedata)에 가면 다른 리뷰도 많다.
* [Designing Data-Intensive Applications by Martin Kleppmann: a review](https://blog.softwaremill.com/designing-data-intensive-applications-by-martin-kleppmann-a-review-20406d7132f9)
* [On Designing Data-Intensive Applications](https://forvillelser.vorce.se/posts/2018-12-30-on-designing-data-intensive-applications.html)

오늘날 우리가 만들고 접하는 많은 소프트웨어들이 많은 양의 데이터를 처리하고 있는데 이 책은 이런 데이터를 처리하는 시스템을 어떻게 디자인 할 것이냐를 주제로 다방면의 지식을 처음부터 차근차근 알려준다. 일단
* 어떻게든 데이터를 어떻게 저장하고 불러올지 정의를 해야 하고 (Ch2. Data Models and Query Languages)
* 램에만 저장하고 끝날 것이 아니니 Btree, SSTable등의 자료구조와 크래시 복구를 위한 WAL 얘기도 나오고 (Ch3. Storage and Retrieval)
* 데이터를 효율적으로 저장하는 포맷 (XML, JSON, Avro, Parquet, Protobuf)과 Schema evolution에 대한 얘기도 해주시고 (Ch4. Encoding and Evolution)
* 데이터가 한 대에 다 안 들어갈테니 머신 여러 대에 나눠서 저장하는 방법 (Ch5, Ch6. Replicatoin & Partitioning)
* 데이터를 여러 명이 동시에 수정하다 보니까 생기는 Transaction 문제 (Ch7)
* 그리고 Reliable한 시스템을 만드려면 결국 분산 시스템을 만들 수 밖에 없는데 왜 그게 어려운지와 (Ch8) 특히 Consistency and Consenus 문제 (Ch9)
* 그리고 데이터를 처리하는 방식 - Batch Processing과 Stream Processing의 개념과 이를 위해 등장한 도구들을 소개하신다. Event sourcing, CQRS 얘기도 같이 나온다.(Ch10, Ch11)

이렇게 다양한 분야의 지식을 훑은 다음에는 이 분야의 미래를 조망하시면서 결국 데이터베이스의 내부가 돌아가는 방식과 현재의 다양한 스택으로 구성된 어플리케이션이나 결국 크게 보면 닮은 점이 많다고 일깨워 주신다 (예를 들면 DB에서 인덱스를 만드는 것은 원본 데이터가 바뀔 때마다 다른 데이터가 바로바로 따라가면서 업데이트되어야 한다는 점에서 결국 팔로워 레플리카를 만드는 것이랑 비슷하다).

하지만 더욱 유연하고 확장성있는 시스템을 위해서는 데이터베이스 내부의 Transaction에 의존하기 보다는 스트림으로 여러 모듈을 잇는 것이 유리하고 (결국 application의 모든 state는 소스 event의 derived state로 바라봐야 하고, 다시 이 state가 다른 event의 src가 됨. 그리고 이게 replicate이 되려면 각 action은 idempotent 해야 함) 이런 식으로 여러 도구들을 쉽게 잇는 쪽으로 기술이 발전할 것이라고 저자는 예측한다. 따져보면 MySQL에 있는 데이터를 ElasticSearch를 통해 검색을 지원하려면 예전에는 rivering을 위한 별도의 툴이 있었지만 지금은 Kafka Connector 등이 나와있고, 앞으로는 유닉스에서 파이프를 통해 작업들을 잇듯이 더 간단하게 같은 작업을 할 수 있도록 진화할 가능성이 높다.

솔직히 이해를 100% 못한 부분도 많았지만 (예를 들자면 메시지 큐가 exactly once delivery를 어떻게 보장하는지...) 그래도 막연히 스파크, 카프카 등등을 잘 익혀야겠다 생각하는 것과, 각각 도구들이 어떤 문제를 풀고자 나왔는지 대략의 이해를 하고 익히는 것은 정말 큰 차이라서 책을 읽으면서 참 낸 돈에 비해 많이 배워간다는 생각이 많이 들었다. 물론 꾸역꾸역 킨들로 읽는데 거의 3달이 걸렸지만 저자는 이 책을 쓰는데 더 많은 시간을 썼을 것이고, 또 이 도구들이 현재의 수준으로 발전하기에는 더 많은 사람들의 노력이 있었을테니 시간이 하나도 아깝지 않다.
