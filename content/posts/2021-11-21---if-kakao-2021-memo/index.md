---
title: if Kakao 2021 메모
date: "2021-11-21"
template: post
slug: /if-kakao-2021-memo
draft: false
category: "dev"
description:
tags:
---

if Kakao 2021에서 관심있는 세션을 몇 개 봤는데 블로그에다가 나름의 생각을 정리해놓으면 나중에 보기가 좋아서 간단히 세션별로 적어본다.

### Commerce Query data Rebuild Success 했을까

https://if.kakao.com/session/106

카카오 선물하기에서는 보낸사람을 PK로해서 DB를 샤딩하는데, 이 선물함이라는 서비스는 본질적으로 받는 사람 위주로 모든게 돌아가서 받는 사람으로 샤딩된 DB가 필요한 상황이셨다고 한다 (모든 샤드에 대해 요청을 날라기에는 다소 부담됨). 기왕 새로 데이터를 구성하는 김에 아예 조인까지 미리 해서 그냥 MongoDB의 다큐먼트 하나만 읽으면 되도록 데이터를 배치 + 스트리밍 방식으로 필요한 필드를 다 조인해서 만들어서 쓰신다고 한다.

이 때 Change stream 두 개를 조인해서 쓰는 방법도 있겠지만... 그러면 좀 고려할게 많아서 그냥 업데이트할 데이터의 PK만 메시지에서 받고, 그 PK를 가지고 필요한 데이터를 다시 원천 RDB에서 조인하는 식으로 최대한 정합성을 맞추셨다고 한다. 사실 업데이트한 DB의 PK만 보낼거면 Debezium도 과해서 그냥 ORM 단에서 변경 이벤트를 발생하는 식으로 구성하셨다고 (이런 실용주의 너무 좋다). 50시간 걸리는 전체 정합성 테스트를 7번해서 결국 데이터 정합성을 맞추셨다는 부분은 리스펙.

데이터 조인하는 부분은 개인적으로는 stateful하게 stream-stream 조인하는 것에 관심이 많았는데 예전에 봤던 우아한 형제들 [MSA 발표](https://www.youtube.com/watch?v=BnS6343GTkY)도 그렇고 실제로는 그냥 '변경' 이벤트만 발생하고 원본을 다시 읽는 형태로 많이 사용하는 것 같다. 사실 원본 데이터 저장소에 가해지는 부하가 과하지 않으면 이 방식이 복구 및 재처리도 깔끔하고 좋다. 결국 변경이 얼마나 자주 일어나냐의 문제.

### BERT보다 10배 빠른 BERT 모델 구축

https://if.kakao.com/session/27

카카오 쇼핑하우에서는 상품 카테고리 분류를 텍스트 기반으로 진행하고 있었는데 기존 모델을 transformer 기반의 모델로 바꾸면서 알게되신 점을 공유해주시는 내용. `상품 분류를 위해 별도의 MLM pretrain을 거친 레이어가 적고 vocab size가 큰 bert 구축` 이라고 쓰면 대략 맞다. Encoder block은 3개, attention heads는 2개, vocab size는 30만, BPE가 아닌 unigram이용, segment embedding은 아예 삭제 등등이 주요 변경 내용이다.

Encoder block을 줄이는 것은 최근에 [Que2Search: Fast and Accurate Query and Document Understanding for Search at Facebook](https://research.fb.com/publications/que2search-fast-and-accurate-query-and-document-understanding-for-search-at-facebook/) 라는 페이퍼를 읽으면서도 만났던 내용이라서 신기했다. 여기서도 빠른 온라인 서빙을 위해서 쿼리를 인코딩 할 때는 2-Layer XLM을 사용했다고 하는데 vocab size 얘기는 없고 대신에 아예 char 3-gram을 임베딩해가지고 XLM의 CLS 토큰이랑 같이 쓰는 부분이 차이점. 결국 각 토큰의 의미를 더 잘 학습시키게 하려는 의도가 아닐까 싶다.

### 은행 전체계좌조회 API Renewal. 오슬로

https://if.kakao.com/session/109

은행에는 계정계라고 찐 핵심 OLTP가 있고, 이걸 가지고 고객에게 보내주는 레이어가 있는데 계좌 조회 같은 것은 계정계가 잠시 중단되어도 가능하도록 Consistent한 Read-only 레이어를 구축하신 내용.

원본 DB에 부하를 줄이기 위해서 오라클의 Golden gate를 사용해서 오슬로용 오라클에 데이터가 실시간으로 복제된다고 한다 (역시 중요한 시스템은 오라클). 하지만 원본 DB의 변경사항이 복제되는 그 찰나의 기간 동안이라도 데이터 정합성이 깨지게 되면 안되므로 일단 원본 DB의 변경이 일어나면 그 변경에 대해서는 레디스에 따로 마킹을 해놓고, 이게 복제가 완료될 때 지우는 식으로 `복제중`인 이벤트의 상태를 관리하신다고 한다. 만약 내 계좌에 `복제중`인 이벤트가 있다? 이러면 오슬로 말고 원래 쓰던 계정계를 보는 방식으로 고객 경험을 유지하신다고.

### 쿠버네티스 레디스 클러스터 구축기

https://if.kakao.com/session/52

stateless 앱들이야 이제 k8s 때문에 확장이 너무 쉽지만 레디스는 그냥 스케일을 고정하는 경우도 많은데, 발표자님은 직접 오퍼레이터를 하나 만드셔서 카카오 TV에서 쓰이는 레디스 클러스터를 온라인으로 스케일 인/아웃하신다고 한다 (AWS에서 ElasticCache에서 노드 더하는 느낌으로). 사실 이거는 더 전사/인프라적인 레벨에서 제공/관리해야하는 기능이라고 생각하지만 그래도 직접 만드셔서 문제없이 서비스에서 쓰시는 것이 참 대단하시다고 느꼈다.

### 스마트 메시지 서비스 개발기

https://if.kakao.com/session/22

광고를 카톡 메시지로 보내는 서비스를 개편하면서 이벤트 기반으로 다시 만드셨다고 한다. 재밌었던 포인트만 몇 개 정리하면

1. 일단 golang으로 작성하셨던 부분. 흔히 대기업 때문에 우리나라 개발자들이 자바 스프링 위주의 경력을 쌓는다는 인식이 많은데 이렇게 스프링을 안 쓰는 사례가 많이 발표되면 다양성에 큰 도움이 될 것 같다. 아예 '스프링 공화국 탈출기'라는 제목의 발표도 있었는데 벌써 유지보수 생각이 드니 내가 이제 꼰대 개발자가 된 것 같다.
2. 예약 스케쥴링을 위해 Rabbit MQ를 사용하신 부분 (이건 RabbitMQ가 잘한다)
3. 카프카 스트림을 이용해서 필터/조인등을 처리하신 부분. 예제는 많이 봤어도 실제 쓰이는 사례는 별로 보지 못해서 반가웠다.
