---
template: post
slug: /google-cloud-speech-api-at-browser
title: Google Cloud Speech API를 브라우저에서 사용하기
date: '2018-07-28'
category: "dev"
tags:
draft: false
description: 오디오 파일이랑 친해져가는 이야기
---
브라우저로 [getUserMedia()](https://www.html5rocks.com/ko/tutorials/getusermedia/intro/)를 가지고 오디오를 캡쳐하면 Blob 형태로 오디오 파일을 얻을 수 있는데, 이 때 크롬에서는 `webm/audio`라는 형식으로 오디오를 캡쳐하고, 파이어폭스에서는 `audio/ogg;codecs=opus` 형태로 오디오를 캡쳐함. ([SO 질문](https://stackoverflow.com/questions/41739837/all-mime-types-supported-by-mediarecorder-in-firefox-and-chrome)).
문제는 IBM Watson Speech Recognition API와 다르게 Google Cloud Speech API는 webm 파일을 지원을 안한다는 것 [지원하는 포맷들](https://cloud.google.com/speech-to-text/docs/reference/rest/v1/RecognitionConfig). 여기서 여러가지 솔루션이 있음.

1. 어차피 연구 프로토타입인데 그냥 파이어폭스로 사용하도록 안내.
2. 그냥 별도의 서버에다가 `webm/audio` 형태로 올리고 거기서 `audio/ogg;codecs=opus`형태로 변환.
3. 브라우저 상에서 다른 오디오 파일 형식으로 변환 후 API를 호출. Web Worker를 써서 많이들 하는 것 같음.

만약 내가 프론트엔드 개발자라면 당연히 3을 택해서 크로스-브라우징되는 솔루션을 고민해야겠지만 지금은 그냥 프로토타입 만들기 바쁜 대학원생이라서 2를 택함. 1을 선택 안 한 이유는.. ~~해봤는데 왜인지 잘 안 되었음~~. 원래 잘 안 되었는데 이거 쓰다가 찜찜해서 해보니까 또 된다. 일단 두 방법을 소개해 본다.

#### 방법1. 파이어폭스로 녹음
파이어폭스에서 녹음된 blob을 [FileReader.readAsDataURL()](https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL)를 이용, base64로 잘 인코딩해준다. 여기서 유의할 점은 이 오디오 파일은 채널이 2개라서 API `v1`으로는 처리가 되지 않고, `v1p1beta`에서 `audioChannelCount`까지 같이 설정해줘야 정상적으로 인식이 된다. 단, 녹음된 오디오의 비트레이트가 44,100이면 인식을 지원하지 않는다. 비트레이트를 바꾸는 방법은..#3을 참조.

#### 방법2. 그냥 서버에서 OGG로 변환
일단 [mediainfo](https://mediaarea.net/ko/MediaInfo/Download)로 크롬에서 녹음된 파일을 뜯어보면
```bash
General
Complete name                            : blob.ogg
Format                                   : WebM
Format version                           : Version 4 / Version 2
File size                                : 21.7 KiB
Writing application                      : Chrome
Writing library                          : Chrome
IsTruncated                              : Yes
FileExtension_Invalid                    : webm

Audio
ID                                       : 1
Format                                   : Opus
Codec ID                                 : A_OPUS
Channel(s)                               : 1 channel
Channel positions                        : Front: C
Sampling rate                            : 48.0 kHz
Bit depth                                : 16 bits
Compression mode                         : Lossy
Language                                 : English
Default                                  : Yes
Forced                                   : No
```
이렇게 나온다. 코덱은 Opus 인데 컨테이너가 WebM 임을 알 수 있다. 우리의 친구 FFMpeg으로 오디오만 Ogg 컨테이너에 담아서 꺼내주면
```
ffmpeg -i webp_name -vn -acodec copy ogg_name
```
```
Complete name                            : blogreal.ogg
Format                                   : Ogg
File size                                : 21.6 KiB
Duration                                 : 3 s 359 ms
Overall bit rate                         : 52.6 kb/s
Writing application                      : Lavf57.83.100

Audio
ID                                       : 608693484 (0x2447ECEC)
Format                                   : Opus
Duration                                 : 3 s 359 ms
Channel(s)                               : 1 channel
Channel positions                        : Front: C
Sampling rate                            : 48.0 kHz
Compression mode                         : Lossy
Writing library                          : Lavf57.83.100
Language                                 : English
```
구글 클라우드 스피치 API에 넣을 수 있는 형태로 잘 바뀐다.

### 느낀점
* Web Worker, WebAssembly 이런게 왜 나오나 몰랐는데 이제 확실히 알 것 같다. 브라우저로 전통적인 작업을 넘어선 범위의 일을 하고 싶으면 저런게 정말 필요한듯.
* 영어 못하면 진짜 개발 어떻게 하지 ???
* 왠지 기다리면 크롬에서 짜잔하고 ogg를 지원하지 않을까 하는 바램이 있다.

### 참고한 글들
아마 이 글을 검색해서 들어오셨을 분들이면 다 읽어보셨겠지만,
* [Record to an Audio File using HTML5 and JS](https://air.ghost.io/recording-to-an-audio-file-using-html5-and-js/) 는 하지 못했던 #3 솔루션에 대해서 자세한 설명을 해주고 계심. 물론 바로 가져다 쓸 수 있는 정도는 아님. 구글 스피치 API 데모는 LINEAR16으로 인코딩을 한 다음에 웹소켓으로 [Streaming Recognize](https://cloud.google.com/speech-to-text/docs/streaming-recognize)를 하는듯.
* [WebRTC to .NetCore API, GoogleSpeechApi return nothing #2135](https://github.com/GoogleCloudPlatform/google-cloud-dotnet/issues/2135) 나랑 무지 유사한 삽질의 흐름을 거치셨음. 근데 마지막에 사용하는 ffmpeg 커맨드가 살짝 다름.
