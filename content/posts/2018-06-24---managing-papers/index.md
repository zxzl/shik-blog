---
template: post
title: N=1 case for managing papers
slug: managing-papers
date: '2018-06-24'
category: "research"
tags:
draft: false
description: 논문을 다운받고 읽고 정리하는 이야기
---

트위터를 넘기다가 Phillip Guo 교수님의 트윗을
<blockquote class="twitter-tweet" data-lang="ko"><p lang="en" dir="ltr">I have a Dropbox folder of random papers and other PDFs that I download whenever I see something interesting. When I’m bored and want to kill time (eg, waiting at airport) I just browse thru that folder.</p>&mdash; Philip Guo (@pgbovine) <a href="https://twitter.com/pgbovine/status/1010726467771043840?ref_src=twsrc%5Etfw">2018년 6월 24일</a></blockquote>

보고 *어 나도 저렇게 하는데!* 하는 생각이 들었다. 평소 읽을 / 읽은 페이퍼를 관리하는 방법에 대해 고민이 많아서 요즘 해보고 있는 방법을 간략히 기록해보려고 한다.

#### 1. 읽을 페이퍼들은 일단 한 폴더에 넣어서 관리한다
개인적으로 Dropbox에 `currently_reading` 이라는 폴더를 만들어 놓고  읽어야 되는 페이퍼를 마치 큐처럼 그 폴더에 넣어놓는 것을 선호한다. 드랍박스에 넣는 이유는 아이패드에서 페이퍼를 읽기 편해서이다. 이런 식으로 일단 한 폴더에 넣어 놓으면 논문을 찾다가 갑자기 읽어야 할 페이퍼가 확 많아졌을 때 페이퍼를 빠뜨리지 않고, 일단 그 페이퍼들을 이 폴더에 다 넣어 놓으면 언젠가 하나씩 다 읽을 수 있어서 좋다.

페이퍼를 넣어 놓기만 하고 안 읽으면 어떡하나? 할 수 있지만 그건 그 시간에 더 중요한 페이퍼를 읽거나, 다른 일을 했다는 뜻이니까 크게 걱정할 일이라고 생각하지는 않는다.

#### 2. 페이퍼를 읽는다
예전에는 [Mendeley](https://www.mendeley.com/) 로 밑줄도 치고 메모도 남겼는데 멘델리의 아이패드 앱이 그렇게 질이 좋지 않고, 또 멘델리 밖에서 그 메모 / 밑줄을 확인할 수 없으므로 PDF 파일에 직접 annotate 하는 것을 선호한다. Annotation을 해놓으면 꼭 메모를 열심히 해놓지 않았더라도 나중에 내가 이 페이퍼에서 어디를 힘줘서 읽었는지 기억해내기가 편해서 귀찮아도 메모랑 밑줄을 꼭 남겨 놓으려고 한다. PDF 리더로는 맥 / 아이패드 모두에서 [PDF Expert](https://pdfexpert.com/)를 제일 선호한다. 특히 아이패드 버전에서 드랍박스 폴더랑 싱크를 해주는 기능이 아주 편리하다.

#### 3. 읽은 페이퍼를 정리한다
다 읽은 페이퍼는 일단 별도의 폴더 (예, `done`. 생각해보니 [GTD](https://ko.wikipedia.org/wiki/Getting_Things_Done)랑 비슷한 감이 있다)에 넣어 놓고 한 번에 정리하거나, 맥북으로 논문을 읽었을 때는 바로 정리한다. 여기서 정리라 함은

1. Mendeley에 등록
우선 논문을 mendeley에 넣어 놓는다. Mendeley에 논문을 넣어 놓으면 제목이나 내용으로 논문을 검색할 때도 좋고, 나중에 인용할 때도 바로 BibTex 를 만들 수 있어서 편리하다.
2. 메모
Mendeley에 페이퍼를 넣어놓는  것과 별도로 페이퍼에 관한 메모를 어딘가 적어 놓는 편인데 보통 프로젝트 때문에 페이퍼를 읽을 때는 프로젝트 폴더에, 논문을 쓸 때는 그 폴더에  관련 논문을 정리하는 폴더를 만들어놓고 *Citation Key (e.g Guo2018) - Title* 밑에 한 줄로 논문의 내용을 적어놓는다. Citation Key를 적는 이유는 말 그대로 Tex에서 인용할 때 그 키를 적어줘야 하기 때문이다.

이렇게 정리가 끝난 논문들은 1에서 말한 읽을 페이퍼를 담아놓는 폴더에서 삭제해준다.

#### 4. (고민) 페이퍼들의 목록을 관리한다
1~3 까지는 어느정도 고민이 끝난 상태인데 이런 식으로 페이퍼가 쌓이면 이 페이퍼들의 목록을 좀 더 체계적으로 관리해야할 필요가 생긴다. 일단 현재는 비슷한 친구들 끼리 소제목을 찾아서 그 소제목 아래에 관련된 페이퍼를 모아 놓는 식으로 만족하고 있다. 서베이 페이퍼를 쓰는 방법을 찾아보면 프로세스를 개선하는데 크게 도움이 될 것 같다.
