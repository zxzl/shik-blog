---
template: post
title: Boolean() in JavaScript
date: '2016-05-26'
category: "dev"
slug: '/boolean-vs-if'
draft: false
---

Uber 에서 만든 react-vis 를 구경하다가 중간에 if()안에 다시 Boolean() 함수를 써서 if(Boolean())으로 값을 검사하는 부분이 있었다 [링크](https://github.com/uber/react-vis/blob/master/src/lib/utils/animation-utils.js#L54)

```javascript
export function applyTransition(props, elements) {
  let { animation } = props
  if (Boolean(animation)) {
    if (!(animation instanceof Object)) {
      animation = DEFAULT_ANIMATION
    }
    return elements.transition().duration(animation.duration)
  }
  return elements
}
```

혹시 Boolean()이 동작이 다를까해서 이것저것 비교해보는 코드를 짜보았으나

```javascript
var values = {
  _true: true,
  _false: false,
  _string: 'hello',
  _stringEmpty: '',
  _number: 1,
  _numberZero: 0,
  _null: null,
  _emptyObject: {},
  _filledObject: { a: 1 },
  _emptyArray: [],
  _filledArray: [1],
}
var valueNames = Object.getOwnPropertyNames(values)

valueNames.forEach(function(valueName, idx) {
  var value = values[valueName]
  if (Boolean(value) == true) console.log(valueName + ' true in Boolean()')
  else console.log(valueName + ' false in Boolean()')

  if (value) console.log(valueName + ' true in if()')
  else console.log(valueName + ' false in if()')
})
```

딱히 evaluation 이 다르지는 않다.

심지어 그 [밑의 다른 함수](https://github.com/uber/react-vis/blob/master/src/lib/utils/animation-utils.js#L73)에서는 그냥 if()를 쓴다. 딱히 용도가 다른 것 같지는 않은데...

```javascript
export function getCSSAnimation(props, style) {
  let { animation } = props
  let transition = 'none'
  if (animation) {
    // Fallback to default animation if no animation is passed.
    if (!(animation instanceof Object)) {
      animation = DEFAULT_ANIMATION
    }
    const keys = Object.keys(style)
    // Create a string of visualized parameters and filter out those, which
    // don't have values.
    const animationKeys = keys
      .filter(key => Boolean(style[key]))
      .map(key => `${key} ${animation.duration / 1000}s`)
    if (animationKeys.length) {
      transition = animationKeys.join(',')
    }
  }
  return {
    transition,
    ...style,
  }
}
```

저 사람들이 코드리뷰를 안 했을리도 없고, 아아아아 진짜 모르겠다ㅜㅜㅜ.
