---
title: edwith 부스트코스 - 파이토치로 시작하는 딥러닝 기초를 듣고
date: "2019-09-15"
template: post
slug: /taking-edwith-dl
draft: false
category: "dev"
description: 제목에 충실한 강의
tags:
---

[강좌 링크](https://www.edwith.org/boostcourse-dl-pytorch)

회사 딥러닝 교육의 선수과목이래서 들어본 코스. DNN, CNN, RNN을 다루며 약 9시간 분량의 동영상 강의와 각 단원 마다 관련한 프로젝트를 하도록 강좌가 이루어져 있다.

### 동영상 강좌
일단 동영상 강의는 [모두의 딥러닝 시즌2](https://deeplearningzerotoall.github.io/season2/)를 기반으로 하고 있다. 실제로 유튜브에서 동일한 목차와 내용의 동영상 강의를 확인할 수 있다. 모두의 딥러닝 시즌2 강의는 어차피 모두의 딥러닝 시즌1 강의가 기존에 있는 만큼 설명이 오래 필요한 내용 (예를 들면 역전파, ResNet의 구조)은 시즌1 영상을 참조하라고 안내를 해주고 실제 네트워크를 PyTorch로 구현할 때 주의해야 할 점을 좀 더 강조해서 알려주는 느낌이다.

단원마다 강의하시는 분들이 다 달라서 내용의 편차가 있지 않을까 했는데 다들 설명도 잘하시고 가르쳐주시는 내용도 실용적이어서 좋았다. 그 중에서 유독 좋았던 클립 몇 개만 꼽아보면

* [1-1 Tensor Manipulation1](https://youtu.be/St7EhvnFi6c) 네트워크 짜는 것도 짜는 건데 그 전에 데이터를 만들면서 numpy array 조작하는게 항상 어렵다고 느꼈는데 이 부분에 대해서 충분히 시간을 써가면서 설명해주셔서 이후 숙제할 때 하나도 어려움이 없었다.
* [10-3 visdom](https://youtu.be/bAPzUjfPAlQ), [10-4 ImageFolder](https://youtu.be/KDVOAjaTnDU) visdom은 Tensorboard 같은 건데 훨씬 유연하게 쓸 수 있는 대시보드이고, ImageFolder는 폴더 형태로 이미지를 구성해놓으면 이를 바로 Image classification 문제의 데이터셋으로 쓸 수 있게 해주는 torchvision의 기능이다. 둘 다 실제 CNN을 어딘가에 써먹으려면 유용한 기능이어서 강좌에 넣어주신게 되게 감사했다.
* [10-6 ResNet](https://youtu.be/Qb_bYWcQXqY) PyTorch의 ResNet 모듈이 어떻게 생겼나 40분동안 설명해주시는 클립으로 저런 네트워크 코드에 대한 심리적 장벽을 낮춰주는 고마운 강의이다. 아무래도 PyTorch여서 그런지 설명을 잘 들으면 생각보다 따라갈만해서 좋은 경험이었다.

전반적으로 수업을 들으면 바닥부터 이해는 아니더라도 PyTorch를 이용해서 원하는 데이터를 기본적인 모델에 넣을 수는 있게 되도록 강좌가 구성되어 있다.

### 프로젝트
프로젝트는 각 단원에서 배운 네트워크 (DNN, CNN, RNN)을 특정 데이터셋에 써먹는 내용으로 되어 있다. 기본적으로 Jupyter Notebook과 데이터셋, 그리고 자가 점검을 위한 스크립트로 구성된 압축 파일을 받아서 혼자 빈칸을 채우는 방식으로 프로젝트가 진행된다.
우선 Jupyter Notebook을 Colab에서도 돌릴 수 있도록 관련 함수 (구글 드라이브 연동) 을 넣어준 것이 최대 장점이라고 생각한다. 이런 강좌를 들으면 항상 일반 Fully Connected Network 까지는 노트북 CPU에서 잘 돌아가다가 다음 단원의 CNN 부터는 트레이닝이 좀 많이 느려지는 문제가 생기는데 Colab을 사용하면 GPU를 사용할 수가 있으니 쾌적하게 프로젝트를 따라갈 수 있었다. 점검 함수를 두어서 사소한 실수로 진행이 턱 막히는 점을 방지하는 점, 블록마다 실제 PyTorch 문서 링크를 달아준 점도 프로젝트를 따라가는데 정말 큰 도움이 되었다. 다만 제공되는 코드에 아주 사소한 오류들이 있었는데 이 부분은 맨 아래에 따로 정리를 하도록 하겠다.

### 결론
제목인 `파이토치로 시작하는 딥러닝 기초`을 정말 잘 알려주는 강좌이다. 예전에 학교 수업도 들어보고, cs231n 수업 영상도 들어봤는데 PyTorch와 관련된 영역에 대해서는 확실히 엣지가 있다고 느꼈다. 100% 한국어로 제공되있는 것도 분명 장점이다.
다만 시기적으로 딥러닝을 공부해야 하는 사람이면 이미 이 정도 내용은 다 알고 있지 않을까…하는 생각이 들기는 하는데 덕분에 나같은 사람이 잘 공부를 할 수 있고, 또 원래 지식이란 것이 원래 점점 보편화가 되는 것이니 이 강의는 그 역할을 충분히 하고 있다고 생각한다.

##### 프로젝트 코드의 사소한 오류들
* 프로젝트B
`check_util/checker.py`에 63번째 줄에 보면 아래와 같은 코드가 있는데 `2000이 아닌 {}` 여기 부분에서 괄호 안을 `len(dataset)`으로 채워줘야 실행시 오류가 나지 않았다.
```python
    try:
        if len(dataset) != 2000:
            print(f'{dataset.mode}의 __len__ 함수가 train_data의 데이터 갯수를 2000이 아닌 {}으로 반환하고 있습니다. 가지고 있는 데이터셋 또는 __len__함수 구현에 문제가 없는지 다시 확인하시기 바랍니다.'.format(len(dataset)))
            len_flag = False
```
* 프로젝트C
노트북의 `6. 베이스라인 성능 측정`에 보면 아래와 같은 블록이 있다
```python
for i in range(15):
    data_idx = np.random.randint(len(test_data))
    pred = test_data[data_idx][0][-1, 0]
    pred = pred * std[0] + mean[0]  # 예측 기온을 normalization 이전 상태(섭씨 단위)로 되돌리는 작업
    target = test_data[data_idx][1][0] * std[0] + mean[0]  # 실제 기온을 normalization 이전 상태(섭씨 단위)로 되돌리는 작업
    print('예측 기온: {:.1f} / 실제 기온: {:.1f}'.format(pred, target))
```
여기서 `std`, `mean`을 전역 변수에서 가져오는데 저런 전역 변수를 정의한 적이 없으므로 에러가 난다. 대신에 `test_data.std[0]`, `test_data.mean[0]`을 사용하자 (아래 블록에도 비슷한 내용이 있는데 여기는 맞게 되어 있다)
