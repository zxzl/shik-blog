---
template: post
title: 번역)Exploring ES6 - 9. Variables and scoping
date: '2016-06-03'
category: "dev"
slug: "/exploring-es6-ch9"
tags:
draft: false
---

공부빼고 모든 것이 재밌어진다는 시험기간의 특성을 적극 활용해서 이곳의 CSS 도 고치고, 그 기념으로 새로 포스팅을 하기로 했다. 뭔가 오리지널 하면서 영양가가 있기가 힘들어서 공부도 할 겸 번역 포스팅을 시도 해보았다. 오늘 번역한 텍스트는 자바스크립트 연구가이신 [Axel Rauschmayer](http://rauschma.de/)님이 쓴 [Exploring ES6](http://exploringjs.com/es6/)라는 online book 의 9 번째 단원이다. 예~전에 혹시 부분부분 번역해도 되냐고 메일을 드렸었는데 아직 답은 얻지 못했다.

추가: 쓸 때는 몰랐는데 이미 전편이 번역되어 있었습니다! [https://github.com/ES678/Exploring-ES6](https://github.com/ES678/Exploring-ES6)

---

<br /><br />

# 9. Variables and scoping

## 9.1 들어가면서

ES6 는 변수를 선언하는 두가지 새로운 문법을 제공합니다. 새롭게 도입된 `let`과 `const`는 ES5 에서 변수를 선언하기 위해 사용되던 `var`을 거의 완벽히 대체할 수 있습니다.

### 9.1.1. `let`

`let`은 `var`과 유사하게 동작하지만, `let`이 선언하는 변수는 *block-scoped*로 현재 블럭에서만 존재합니다. `var`은 *function-scoped*였습니다.

다음 코드에서 `let`으로 선언된 변수인 `tmp`는 line A 에서 시작하는 블럭에서만 존재하는 것을 확인할 수 있습니다.

```javascript
function order(x, y) {
  if (x > y) {
    // (A)
    let tmp = x
    x = y
    y = tmp
  }
  console.log(tmp === x) // ReferenceError: tmp is not defined
  return [x, y]
}
```

### 9.1.2 `const`

`const`는 `let`과 유사하게 동작하지만, 변수를 선언 즉시 바로 초기화해줘야 하고 이후로 그 내용(value)을 변경할 수 없습니다.

```javascript
const foo;
    // SyntaxError: missing = in const declaration

const bar = 123;
bar = 456;
    // TypeError: `bar` is read-only
```

`for-of`는 loop 가 돌 때마다 변수를 선언하므로, const 로 loop 변수를 선언하는 것도 가능합니다.

```javascript
for (const x of ['a', 'b']) {
  console.log(x)
}
// Output:
// a
// b
```

### 9.1.3 변수를 선언하는 방법들

아래 표는 ES6 에서 변수를 선언하는 6 가지 방법에 대해 정리해 놓은 것입니다.

![6 ways of declaring variables]({{ site.baseurl }}/images/var-cheatsheet.png)

## 9.2 `let`과 `const`를 통한 block scoping

`let`과 `const` 둘 다 *block-scoped*된(변수를 감싸는 가장 안쪽의 블럭에서만 존재하는) 변수를 선언합니다. 아래의 코드에 `const`로 선언된 `tmp` 변수는 if 문의 then-block 에서만 존재합니다.

```javascript
function func() {
  if (true) {
    const tmp = 123
  }
  console.log(tmp) // ReferenceError: tmp is not defined
}
```

반면 `var`로 선언된 변수는 _function-scoped_ 되어있습니다.

```javascript
function func() {
  if (true) {
    var tmp = 123
  }
  console.log(tmp) // 123
}
```

Block scoping 에서는 함수 내에서 잠시 변수를 덮는 것(원문: shadow)도 가능합니다.

```javascript
function func() {
  const foo = 5;
  if (···) {
     const foo = 10; // shadows outer `foo`
     console.log(foo); // 10
  }
  console.log(foo); // 5
}
```

## 9.3 `const`는 불변하는(immutable) 변수를 만듭니다

`let`으로 선언한 변수는 변경가능(mutable)합니다.

```javascript
let foo = 'abc'
foo = 'def'
console.log(foo) // def
```

`const`로 선언된 상수는 불변, 다른 값을 할당할 수 없습니다.<br />
(strict 모드와 상관없이 `TypeError`를 내놓습니다)

```javascript
const foo = 'abc'
foo = 'def' // TypeError
```

### 9.3.1 함정: `const`는 변수의 값을 불변하게 만들어주지는 않습니다

`const`는 변수가 항상 같은 값을 가지고 있다는 뜻일 뿐, 그 값 자체가 변할 수 없다는 뜻은 아닙니다. 아래의 예에서 `obj`는 불변이지만 그것이 가리키고 있는 값은 변할 수 있어서 우리가 속성을 더할 수 있습니다.

```javascript
const obj = {}
obj.prop = 123
console.log(obj.prop) // 123
```

물론 `obj`에 다른 값을 할당하는 것은 불가능합니다.

```
obj = {}; // TypeError
```

만약 `obj`의 값 자체를 불변하게 하고 싶을 때는 [freeze](http://speakingjs.com/es5/ch17.html#freezing_objects) 등을 이용할 수 있습니다.

```javascript
const obj = Object.freeze({})
obj.prop = 123 // TypeError
```

#### 9.3.1.1 함정: `Object.freeze()`는 `shallow`합니다.

`Object.freeze()`가 _shallow_, 즉 대상의 `property`만을 `freeze` 할 뿐 `object`로 되어있는 `property`까지 `freeze`하지는 않습니다. 아래 코드에서 `obj`는 `freeze`되있지만,

```javascript
> const obj = Object.freeze({ foo: {} });
> obj.bar = 123
TypeError: Can't add property bar, object is not extensible
> obj.foo = {}
TypeError: Cannot assign to read only property 'foo' of #<Object>
```

`obj.foo`는 그렇지 않습니다.

```javascript
> obj.foo.qux = 'abc';
> obj.foo.qux
'abc'
```

### 9.3.2 Loop body 안에서의 `const`

`const` 변수가 한 번 선언되고나면 그 값(무엇을 가리키고 있는지)는 변할 수 없습니다. 하지만 그렇다고 해서 scope 에 다시 들어가 변수를 새롭게 선언할 수 없는 것은 아닙니다. 아래 코드를 참고해 주세요.

```javascript
function logArgs(...args) {
  for (const [index, elem] of args.entries()) {
    const message = index + '. ' + elem
    console.log(message)
  }
}
logArgs('Hello', 'everyone')

// Output:
// 0. Hello
// 1. everyone
```

## 9.4 Temporal dead zone

`let`과 `const`로 선언된 변수는 `temporal dead zone`(TDZ)를 가지고 있습니다. 변수의 scope 에 들어갔더라도 선언부분이 실행되기 전에는 접근(`get` 또는 `set`)이 불가능합니다. TDZ 가 없는 `var`로 선언된 변수(이하 `var`변수)와 TDZ 를 지닌 `let`으로 선언된 변수(`let`변수)의 생애주기를 비교해봅시다.

### 9.4.1 `var`로 선언된 변수의 생애주기

`var`변수에는 TDZ 가 없습니다. `var`변수의 생애는 다음과 같습니다.

1.  `var`변수의 scope(둘러싸고 있는 **함수**)에 진입하면, 변수를 위한 저장소(binding)가 생성되며 변수는 undefined 로 즉시 초기화됩니다.
2.  선언부분이 실행되면 `var`변수는 `initializer`의 값을 가지게 됩니다. 만약 값이 주어지지 않았다면 변수는 계속해서 `undefined`값을 가지고 있습니다.

### 9.4.2 `let`으로 선언된 변수의 생애주기

`let`변수는 TDZ 를 지니고 있습니다. `let`변수의 생애는 다음과 같습니다.

1.  `let`변수의 scope(둘러싸고 있는 **블럭**)에 진입하면, 변수를 위한 저장소(binding)가 생성되지만 초기화는 되지 않습니다.
2.  초기화되지 않은 변수에 접근하고자하면 `ReferenceError`가 발생합니다.
3.  선언부분이 실행되면 `let`변수는 `initializer`의 값을 가지게 됩니다. 만약 값이 주어지지 않았다면 변수는 이제부터 `undefined`값을 가지고 있습니다.

`const` 변수는 let 변수와 유사하게 동작하지만 initializer 를 꼭 가지고 있어야합니다.

### 9.4.3 예시

TDZ 안에서 변수에 접근하려하면 예외가 발생합니다.

```javascript
let tmp = true
if (true) {
  // enter new scope, TDZ starts

  // Uninitialized binding for `tmp` is created
  console.log(tmp) // ReferenceError

  let tmp // TDZ ends, `tmp` is initialized with `undefined`
  console.log(tmp) // undefined

  tmp = 123
  console.log(tmp) // 123
}
console.log(tmp) // true
```

Initializer 가 있을 경우 TDZ 는 assignment 가 이루어진 후에 끝납니다.

```javascript
let foo = console.log(foo) // ReferenceError
```

아래의 코드는 TDZ 가 _spatial_(공간 기반)하지 않고 _temporal_(시간 기반)함을 보여줍니다.

```javascript
if (true) {
  // enter new scope, TDZ starts
  const func = function() {
    console.log(myVar) // OK!
  }

  // Here we are within the TDZ and
  // accessing `myVar` would cause a `ReferenceError`

  let myVar = 3 // TDZ ends
  func() // called outside TDZ
}
```

### 9.4.4 `typeof`는 TDZ 안에 있는 변수에 대해 `ReferenceError`를 발생합니다.

만약 TDZ 안에 있는 변수를 typeof 를 통해 접근하고자하면 예외가 발생합니다.

```javascript
if (true) {
  console.log(typeof foo) // ReferenceError (TDZ)
  console.log(typeof aVariableThatDoesntExist) // 'undefined'
  let foo
}
```

이유를 생각해봅시다. `foo`는 아직 선언되지 않았고, 따라서 foo 는 초기화되지도 않았습니다(`let`변수니까). 프로그래머는 `foo`의 존재에 대해 혼동을 하고 있습니다. 따라서 경고를 주는 것이 바람직해보입니다.

또한 이런 류의 검사는 조건부로 전역 변수를 선언하는 경우에도 유용합니다. 이는 오로지 숙련된 자바스크립트 프로그래머들만이 해야하는 일이며 `var`를 통해 할 수 있습니다. `typeof`를 이용하지 않고도 전역 변수가 존재하는지를 확인할 수 있습니다.

```javascript
// With `typeof`
if (typeof someGlobal === 'undefined') {
    var someGlobal = { ··· };
}

// Without `typeof`
if (!('someGlobal' in window)) {
    window.someGlobal = { ··· };
}
```

가첫번째 방법은 global scope 에서만 유효하며 ES6 모듈 안에서는 사용될 수 없습니다.<br />
(역자 주: 꼭 es6 아니어도 `function(){ {코드복붙} }` 하는 도구들에서는 다 안될듯..)

### 9.4.5 TDZ 는 왜 있나요?

* 프로그래밍 에러를 잡기 위해서: 변수의 선언 전에 접근이 가능한 것은 바람직하지 않습니다. 이런 경우는 보통 실수인 경우가 많고 경고를 받는 것이 좋습니다.
* `const`를 위해: `const`를 옳게 동작하도록 만들기는 어렵습니다. [Allen Wirfs-Brock 를 인용하겠습니다](https://mail.mozilla.org/pipermail/es-discuss/2012-September/024996.html). <br /> "TDZ 는...`const`의 합리적 의미(rational semantics)를 부여합니다. 이 주제에 대한 상당한 기술적 논의가 있었고 그 결과 TDZ 가 제일 좋은 방법으로 생겨났습니다. `let` 역시 TDZ 를 가지고 있어 `let`과 `const`를 바꾸어 써도 예상치 못하게 동작이 변하는 일이 없습니다."
* (미래에 나올 수도 있는) guard 를 위해: 자바스크립트는 언젠가 _guards_ - 런타임에 변수가 옳은 값을 가지고 있는지를 검사하는 메커니즘(타입체크를 생각해보세요) - 를 가지게 될 수도 있습니다. 만약 변수가 선언되기도 전에 `undefined` 값을 가지고 있으면 가드가 보장해주는 내용과 충돌이 일어날 수 있습니다.
  (역자 주: 저처럼 guard 에 대해 생소하신 분들은 Swift 쪽에서 찾아보시면 참고할만한 글이들 많습니다. [예](http://rhammer.tistory.com/74) )

### 9.4.6 읽을 거리들

* [“Performance concern with let/const”](https://esdiscuss.org/topic/performance-concern-with-let-const)
* [“Bug 3009 – typeof on TDZ variable”](https://bugs.ecmascript.org/show_bug.cgi?id=3009)

## 9.5 loop 머리(loop heads)에서의 `let`과 `const`들

(역자 주: loop head 는 for (_초기문_; _조건문_; _증감문_) <-까지를 말합니다. 'loop 위'로 번역한 [글](https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Statements/for)도 있었습니다)

다음 loop 문의 머리에서는 변수 선언이 가능합니다.

* `for`
* `for-in`
* `for-of`

변수 선언을 위해서는 `var`, `let`, `const` 중 하나를 사용할 수 있습니다. 각각이 어떻게 다른 결과를 내놓는지 설명해드리겠습니다.

### 9.5.1 `for` loop

loop 머리에서 `var`를 통해 변수를 선언하면 _binding_(저장 공간) 하나가 생깁니다.

```javascript
const arr = []
for (var i = 0; i < 3; i++) {
  arr.push(() => i)
}
arr.map(x => x()) // [3,3,3]
```

3 개의 arrow function 안에 있는 3 개의 `i`는 모두 동일한 binding 을 가리키고 있고, 따라서 모두 같은 값을 출력합니다.

만약 `let`을 이용해서 변수를 선언했다면 loop 가 돌 때 마다 새로운 binding 이 생깁니다.

```javascript
const arr = []
for (let i = 0; i < 3; i++) {
  arr.push(() => i)
}
arr.map(x => x()) // [0,1,2]
```

이번에는 각각의 `i`가 각각의 iteration 의 binding 을 가리키고 있고 그 당시의 값을 지니고 있습니다. 따라서 3 개의 함수는 다른 값들을 출력합니다.

`const`는 `var`처럼 동작하지만 `const`로 선언된 변수의 값은 변경할 수 없습니다.

```javascript
// TypeError: Assignment to constant variable
// (due to i++)
for (const i = 0; i < 3; i++) {
  console.log(i)
}
```

iteration 마다 새로운 binding 이 생기는 것이 이상해보일 수 있지만 loop 변수를 참조하는 함수들을 만들기 위해 loop 를 쓰는 경우에는 아주 유용합니다. 다음 섹션에서 더 자세히 설명드리겠습니다.

### 9.5.2 `for-of` loop 과 `for-in` loop

`for-of` loop 내에서 `var`는 binding 하나를 만듭니다

```javascript
const arr = []
for (var i of [0, 1, 2]) {
  arr.push(() => i)
}
arr.map(x => x()) // [2,2,2]
```

`let`은 iteration 한 번 마다 binding 하나를 만듭니다

```javascript
const arr = []
for (let i of [0, 1, 2]) {
  arr.push(() => i)
}
arr.map(x => x()) // [0,1,2]
```

`const`는 `var`처럼 동작하지만 `const`로 선언된 변수의 값은 변경할 수 없습니다.

`for-in` loop 내부에서의 동작은 `for-of`와 유사합니다.

(역자주: 스펙에 관한 부분은 생략했습니다. http://exploringjs.com/es6/ch_variables.html#_for-loop-per-iteration-bindings-in-the-spec)

### 9.5.3 iteration 마다 binding 이 생기는 것이 왜 유용한가요?

다음은 링크 3 개를 보여주는 HTML 페이지입니다.

1.  "yes"를 클릭하면 "ja"로 번역됩니다
2.  "no"를 클릭하면 "nein"로 번역됩니다
3.  "perhaps"를 클릭하면 "vielleicht"로 번역됩니다.

```html
 <!doctype html>개
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
    <div id="content"></div>
    <script>
        const entries = [
            ['yes', 'ja'],
            ['no', 'nein'],
            ['perhaps', 'vielleicht'],
        ];
        const content = document.getElementById('content');
        for (let [source, target] of entries) { // (A)
            content.insertAdjacentHTML('beforeend',
                `<div><a id="${source}" href="">${source}</a></div>`);
            document.getElementById(source).addEventListener(
                'click', (event) => {
                    event.preventDefault();
                    alert(target); // (B)
                });
        }
    </script>
</body>
</html>
```

line B 에 있는 `target`변수에 의해서 보여질 결과물이 정해집니다. 만약 line A 에서 `let`대신 `var`가 쓰였다면, 루프 전체에 대해서 binding 하나가 생기고 그 값은 결국 마지막으로 저장된 'vielleicht'가 될 것입니다. 따라서 우리가 어느 링크를 누르던 'vielleicht'이 출력될 것입니다.

하지만 고맙게도 `let`을 통해서 iteration 마다 binding 이 생기고, 번역 결과가 올바르게 출력됩니다.

## 9.6 패러미터

### 9.6.1 패러미터 vs. 지역 변수

만약 패러미터와 같은 이름을 가진 변수를 `let`으로 선언하면 static(load-time)에러가 발생합니다

```javascript
function func(arg) {
  let arg // static error: duplicate declaration of `arg`
}
```

같은 일을 블럭 안에서하면 패러미터를 잠시 가립니다(shadow)

```javascript
function func(arg) {
  {
    let arg // shadows parameter `arg`
  }
}
```

반면 패러미터와 같은 이름을 가진 변수를 `var`으로 선언할 경우, 아무 일도 일어나지 않습니다. 이는 같은 scope 에서 `var`변수를 새로 선언하면 아무 일도 일어나지 않는 것과 같은 원리입니다.

```javascript
function func(arg) {
  var arg // does nothing
}

function func(arg) {
  {
    // We are still in same `var` scope as `arg`
    var arg // does nothing
  }
}
```

### 9.6.2 패러미터 기본값과 TDZ

만약 패러미터가 기본 값들을 가지고 있으면 이들은 let 선언의 연속처럼 처리되고 TDZ 를 지닙니다.

```javascript
// OK: `y` accesses `x` after it has been declared
function foo(x = 1, y = x) {
  return [x, y]
}
foo() // [1,1]

// Exception: `x` tries to access `y` within TDZ
function bar(x = y, y = 2) {
  return [x, y]
}
bar() // ReferenceError
```

### 9.6.3 패러미터 기본값은 함수 body 의 scope 를 따르지 않습니다

패러미터 기본값의 scope 는 함수 body 와 별도의 scope 를 가지고 있습니다(기본값의 scope 가 body 를 감쌉니다). 따라서 함수 패러미터 기본값 내의 method 나 함수는 body 의 지역 변수를 볼 수 없습니다.

```javascript
const foo = 'outer'
function bar(func = x => foo) {
  const foo = 'inner'
  console.log(func()) // outer
}
bar()
```

## 9.7 전역 객체

자바스크립트의 [전역객체](http://speakingjs.com/es5/ch16.html#global_object) (브라우저에서의 `window`, Node.js 에서는 `global`) 는 기능이라기 보다는 버그에 가깝습니다(!), 특히 성능 측면에서는 더욱 그렇습니다. 때문에 ES6 에서는 다음과 같은 구분이 도입되었습니다.

* Global object 의 모든 property 는 global variable 입니다. Global scope 에서 다음을 이용한 선언이 그러한 property 를 만듭니다.
  * `var`를 이용한 선언
  * 함수 선언
* 하지만 이제 Global object 의 property 가 아닌 global variable 들이 존재합니다. Global scope 에서 다음을 이용한 선언이 그러한 variable 을 만듭니다.
  * `let`을 이용한 선언
  * `const`를 이용한 선언
  * Class 선언

(역자 주: 이해가 잘 안되서 다음과 같은 느낌일까?하고 코드를 짜봤는데 babel 에서 let 이 var 로 바로 바뀌는 바람에 실험을 못했습니다)

```javascript
var a = 1
let b = 1
console.log(window.a)
console.log(window.b)
```

## 9.8 함수 선언과 클래스 선언

함수 선언은...

* _block-scoped_ 입니다, 마치 let 처럼
* (Global scope 에서) `global`객체의 property 를 만듭니다. 마치 var 처럼
* *hoist*됩니다. 해당 scope(여기서는 block 단위) 어디서 함수가 선언되었는지와 상관없이 scope 의 시작과 함께 생성됩니다.

다음 코드는 함수 선언이 *hoist*됨을 보여줍니다.

```javascript
{
  // Enter a new scope

  console.log(foo()) // OK, due to hoisting
  function foo() {
    return 'hello'
  }
}
```

클래스 선언은...

* _block-scoped_ 입니다.
* `global` 객체에 property 를 만들지 않습니다.
* *hoist*되지 않습니다.

클래스 선언이 *hoist*되지 않는다는 사실이 놀라울 수 있습니다. 왜냐하면 결국 클래스 선언 역시 함수를 만들기 때문입니다. 하지만 `extends` 부분에서 다른 변수나 함수를 참조해야하기 때문에 클래스 선언은 적절한 시점에 실행되어야 합니다.

```javascript
{
  // Enter a new scope

  const identity = x => x

  // Here we are in the temporal dead zone of `MyClass`
  const inst = new MyClass() // ReferenceError

  // Note the expression in the `extends` clause
  class MyClass extends identity(Object) {}
}
```

## 9.9 코딩 스타일: `const` vs. `let` vs. `var`

항상 `let`과 `const` 중 하나를 이용하도록 하세요.

**1)** 일단 `const`를 고려하세요. 값이 절대 변하지 않을 경우 - 해당 변수가 절대 assignment 의 왼쪽에 있거나 ++, -- 되지 않을 경우 - 에 `const`를 이용할 수 있습니다. `const` 변수가 가리키고 있는 객체의 수정은 허용되어 있습니다.

```javascript
const foo = {}
foo.prop = 123 // OK
```

심지어 `const`를 `for-of`에서 사용할 수도 있습니다. Iteration 마다 (불변하는)binding 이 생기기 때문이지요.

```javascript
for (const x of ['a', 'b']) {
  console.log(x)
}
// Output:
// a
// b
```

for 문 안에서 x 는 수정될 수 없습니다.

**2)** 아니라면, `let`을 사용하세요 - 변수의 초기값이 나중에 바뀔 경우에

```javascript
let counter = 0 // initial value
counter++ // change

let obj = {} // initial value
obj = { foo: 123 } // change
```

**3)** `var`는 쓰지 마세요

만약 위의 원칙을 따른다면 `var`는 레거시 코드에서만 나타나 *리팩토링시 신중함이 요구됨*을 알릴 것입니다.

`let`이나 `const`는 할 수 없고, `var`는 할 수 있는 일이 있습니다: (global scope 에서) `var`로 선언된 변수는 `global`객체의 property 가 됩니다. 하지만 보통 이는 좋은 일이 아닙니다. 필요한 경우 `window`나 `global`에 property 를 assign 해서도 같은 일을 할 수 있습니다.

### 9.9.1 다른 접근

위의 규칙의 대안으로 `const`를 completely immutable 한 경우에만(primitive value 나 frozen object) 이용하는 방법이 있습니다. 이제 우리는 두가지 옵션을 가지고 있습니다.

1.  일단 `const`(추천): `const`는 immutable 한 **binding**을 뜻합니다
2.  일단 `let`(대안): `const`는 immutable 한 **value**를 뜻합니다

둘 간의 우열을 가리기는 힘듭니다. 다만 저는 1 번을 아주 약간 선호합니다.
<br /><br /><br /><br />

---

번역 후기

1.  const 쪽 번역이 약간 이상한데 예제코드가 워낙 명료해서 이해에는 큰 지장이 없으리라 믿는다.
2.  왜 JS 가 욕먹는지 알겠다. 말이 TDZ 지 사실 다른 언어에서는 너무나 자연스러운 것 아닌가...
3.  번역이 강력한 학습 방법이긴 한데, 가장 효율적인지는 의문이 남는다.
