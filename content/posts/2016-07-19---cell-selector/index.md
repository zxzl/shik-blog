---
template: post
title: 꼭 해봐야 알까
date: '2016-07-19'
category: "dev"
slug: "/cell-selector"
tags:
draft: false
---

학기 중에는 학기라고해서 SNUTT 리뉴얼 작업을 소흘히 했는데, 정작 방학이 시작하니까 뒤늦게 대학원을 체험하겠다고 시작한 랩인턴이 생각보다 바빠서 꾸준히 커밋을 하지 못했다. 그래도 좀있으면 수강신청인데 그때까지 완성은 하지 못하더라도 조금씩 진전은 보여야 같이 작업하는 동생분들에게 덜 미안할 것 같아 그동안의 난제였던 특정 시간대를 선택해서 검색하는 부분을 짜보기로 했다. 사실 이 부분은 예전에 한 번 짜서 잘 돌아갔었는데, 코드가 너무나도 지저분해서 다시 짜는 것이다.

스펙?은 간단하다. 그냥 빈칸에 마우스를 드래그하면 칸이 선택되고, 선택된 칸에서 드래그를 시작하면 삭제 모드가 되어 선택이 취소된다. Redux 코드들에서 많이 하는대로

```javascript
const TableState = {
  SELECTING: 'SELECTING',
  DELETING: 'DELETING',
  CALM: 'CALM',
}

const CellState = {
  EMPTY: 'EMPTY',
  SELECTED: 'SELECTED',
  SELECTING: 'SELECTING',
  DELETING: 'DELETING',
  UNABLED: 'UNABLED',
}
```

테이블과 셀의 상태를 미리 정해놓고,

```javascript
this.handleMouseDown = this.handleMouseDown.bind(this)
this.handleMouseUp = this.handleMouseUp.bind(this)
this.handleMouseEnter = this.handleMouseEnter.bind(this)
this.state = {
  tableState: TableState.CALM,
  dragInit: { row: -1, col: -1 },
  cellStateTable: new Array(this.props.row).fill(
    new Array(this.props.col).fill(CellState.EMPTY)
  ),
}
```

모듈이 드래그가 최초로 시작하는 지점, 테이블의 전체 상태, 그리고 각각의 셀의 상태를 state 로 가지고서, 셀들에 이벤트가 발생할 때 마다 `셀의 상태` X `테이블의 상태`로 다음 상태를 만들어 주면 될 것 같았다.

그래서 실제 잘되는 것 처럼 보였으나...

![오오](./cells-looks-good.gif)

영역이 커졌다가 줄어들 경우 한 번 'SELECTING'나 'DELETING'으로 변한 셀들이 다시 원래 상태로 돌아갈 방법이 없다.

![헐](.//cells-crap-indeed.gif)

결국 셀들의 선택 여부와 드래그된 셀들의 상태는 서로 다른 표에 저장하거나, 00, 01, 10, 11 이런 식으로던 분리를 했어야했는데 처음에 그 생각을 못하고, 일단 짜고나서야 옳은 방향을 깨달을 수 있었다.

요즘 하는 인턴도 마찬가지. 교수님들과 주변 분들이 다들 훌룡하시고, 대전의 환경도 너무 좋지만 사실 4 년 내내 연구와 담을 쌓고 살다가 갑자기 흥미를 보일 확률은 적고, 해보니까 실제로도 그렇다. 그래도 막연히 생각하는 것과, 와서 살살이라도 경험해보는 것에는 큰 차이가 있다.

원래 Iterative 한 접근을 좋아하고, 삶도 그래야한다고 생각하지만 한 사람에게 주어진 시간은 유한한 것도 사실이다. 해보지 않고 알 수 없다면, 많이 하는 수밖에.

update) 결국 셀이 선택됐나, 안됐나만 저장하고 드래깅여부는 그냥 시작~끝만 저장한 후 그때그때 체크하는 것으로 바꿨다고 합니다ㅠㅜ. 어차피 드래그할때마다 바뀌는 정보라서 따로 저장할 필요를 못느꼈다고...
