---
template: post
title: Just another tmux cheatsheet
date: '2016-02-29'
category: "dev"
slug: "/tmux-cheat-sheet"
tags:
draft: false
---

**tmux**를 써보기 시작했는데 어떤 cheatsheet 는 너무 간단하고, 어떤 것은 또 너무 방대해서 한 번이라도 써본 커맨드를 적어보았다. 문자 하나만 적혀있는 경우는 `Ctrl + B`를 누른 다음의 키.

#### Managing Sessions

create

```sh
$ tmux new -s sesseion_name
```

rename

```sh
$ tmux rename-session new-name
$
```

attach

```sh
$ tmux attach -t session-name
```

detach

```sh
d
```

#### Managing Windows

create

```sh
$ tmux new-window
c
```

move

```sh
$ tmux select-window -t :0-9
0-9
```

rename current window

```sh
$ tmux rename-window
,
```

delete (current one)

```sh
$ tmux kill-window -t :0-9
&
```

#### Managing Panes

split horizontally (Vim 에서 `:vs`)

```sh
%
```

split vertically (Vim 에서 `:split`)

```sh
"
```

show numbers

```
q
```

Enter scroll mode

```sh
[
```

(exit: q)

move

```sh
<arrows>
```

exit

```sh
d
```
